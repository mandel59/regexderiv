module Main where

import Criterion.Main
import Control.DeepSeq
import Control.Parallel.Strategies
import System.Random
import Regex

main :: IO ()
main = do
  g <- getStdGen
  ls <- return $! map (25000*) [1..10]
  ws <- return $! (map (\n -> replicate n 'a') ls `using` rdeepseq)
  let rs = [ ("(aa|a)*", (Rep True (Alt (Seq (Chr 'a') (Chr 'a')) (Chr 'a'))))
           , ("(a*)*", (Rep True (Rep True (Chr 'a'))))
           ]
      f r w = unNub (test r w)
      --f r w = (test r w :: [()])
  defaultMain $ flip map rs $ \(n, r) ->
    bgroup n $ flip map (zip ls ws) $ \(l, w) ->
    bench ("w" ++ show (length w)) $ nf (f r) w
