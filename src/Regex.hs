{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE TypeFamilies #-}

module Regex where

import GHC.Prim (Constraint)
import Control.Monad.Fix (fix)
import Data.List (nub)

class RFunctor f where
  type RSubCats f a :: Constraint
  type RSubCats f a = ()
  rfmap :: (RSubCats f a, RSubCats f b) => (a -> b) -> f a -> f b  

class RFunctor m => RMonad m where
  rpure :: RSubCats m a => a -> m a
  rbind :: (RSubCats m a, RSubCats m b) => m a -> (a -> m b) -> m b

class RMonad m => RMonadPlus m where
  rmzero :: RSubCats m a => m a
  rmplus :: RSubCats m a => m a -> m a -> m a

rguard :: (RMonadPlus m, RSubCats m ()) => Bool -> m ()
rguard False = rmzero
rguard True = rpure ()

instance RFunctor [] where
  rfmap = map

instance RMonad [] where
  rpure = return
  rbind m k = m >>= k

instance RMonadPlus [] where
  rmzero = []
  rmplus x y = x ++ y

newtype Nub a = Nub { unNub :: [a] }
              deriving (Show, Eq, Ord)

enNub :: (Eq a) => [a] -> Nub a
enNub = Nub . nub

instance RFunctor Nub where
  type RSubCats Nub a = Eq a
  rfmap f = enNub . map f . unNub

instance RMonad Nub where
  rpure x = Nub [x]
  rbind m k = enNub (unNub m >>= (unNub . k))

instance RMonadPlus Nub where
  rmzero = Nub []
  rmplus x y = enNub (unNub x ++ unNub y)

data Reg
  = Eps
  | Chr Char
  | Alt Reg Reg
  | Seq Reg Reg
  | Rep Bool Reg
  deriving (Show, Eq, Ord)

delta_eps' :: (RMonadPlus m, RSubCats m (Maybe Reg)) => (Reg -> m (Maybe Reg)) -> Reg -> m (Maybe Reg)
delta_eps' _ Eps = rpure Nothing
delta_eps' _ (Chr _) = rmzero
delta_eps' d (Alt p q) = d p `rmplus` d q
delta_eps' d (Seq p q) = d p `rbind` (\_ -> d q)
delta_eps' d (Rep True r) = d r `rmplus` rpure Nothing
delta_eps' d (Rep False r) = rpure Nothing `rmplus` d r

delta_eps :: (RMonadPlus m, RSubCats m (Maybe Reg)) => Reg -> m (Maybe Reg)
delta_eps = fix delta_eps'

delta_seq' :: (RMonad m, RSubCats m (Maybe Reg)) => (Reg -> m (Maybe Reg)) -> Reg -> Reg -> m (Maybe Reg) -> m (Maybe Reg)
delta_seq' d p q dq = d p `rbind` maybe dq (\r -> rpure (Just (Seq r q)))

delta' :: (RMonadPlus m, RSubCats m (Maybe Reg)) => Char -> (Reg -> m (Maybe Reg)) -> Reg -> m (Maybe Reg)
delta' _ _ Eps = rpure Nothing
delta' a _ (Chr c)
  | c == a = rpure (Just Eps)
  | otherwise = rmzero
delta' _ d (Alt p q) = d p `rmplus` d q
delta' _ d (Seq p q) = delta_seq' d p q (d q)
delta' _ d (Rep True r) = delta_seq' d r (Rep True r) (rpure Nothing) `rmplus` rpure Nothing
delta' _ d (Rep False r) = rpure Nothing `rmplus` delta_seq' d r (Rep True r) (rpure Nothing)

delta :: (RMonadPlus m, RSubCats m (Maybe Reg)) => Char -> Reg -> m (Maybe Reg)
delta a r = fix (delta' a) r

tau :: (RMonadPlus m, RSubCats m Reg) => Maybe Reg -> m Reg
tau = maybe rmzero rpure

deriv :: (RMonadPlus m, RSubCats m Reg, RSubCats m (Maybe Reg)) => Char -> Reg -> m Reg
deriv a r = delta a r `rbind` tau

trans :: (RMonadPlus m, RSubCats m Reg, RSubCats m (Maybe Reg)) => String -> m Reg -> m (Maybe Reg)
trans [] m = m `rbind` delta_eps
trans (a:w) m = let m' = m `rbind` deriv a in m' `seq` trans w m'

emurate :: (RMonadPlus m, RSubCats m (), RSubCats m Reg, RSubCats m (Maybe Reg), RSubCats m String) => String -> Reg -> m String
emurate [] r = delta_eps r `rbind` (\_ -> rpure "")
emurate (a:w) r = delta a r `rbind` maybe (rpure "") (emurate w)

test :: (RMonadPlus m, RSubCats m (), RSubCats m Reg, RSubCats m (Maybe Reg)) => Reg -> String -> m ()
test r w = trans w (rpure r) `rbind` (\_ -> rpure ())

testr :: (RMonadPlus m, RSubCats m (), RSubCats m Reg, RSubCats m (Maybe Reg), RSubCats m String) => Reg -> String -> m ()
testr r w = emurate w r `rbind` (rguard . (== ""))

denot' :: (RMonadPlus m, RSubCats m String) => (Reg -> String -> m String) -> Reg -> String -> m String
denot' _ Eps w = rpure w
denot' _ (Chr _) [] = rmzero
denot' _ (Chr a) (c:w')
  | c == a = rpure w'
  | otherwise = rmzero
denot' d (Alt p q) w = d p w `rmplus` d q w
denot' d (Seq p q) w = d p w `rbind` d q
denot' d (Rep True r) w
  = (d r w `rbind` \w' -> if w == w' then rpure w else d (Rep True r) w')
    `rmplus` rpure w
denot' d (Rep False r) w
  = rpure w
    `rmplus` (d r w `rbind` \w' -> if w == w' then rpure w else d (Rep False r) w')

denot :: (RMonadPlus m, RSubCats m String) => Reg -> String -> m String
denot = fix denot'

testDenot :: (RMonadPlus m, RSubCats m String, RSubCats m ()) => Reg -> String -> m ()
testDenot r w = denot r w `rbind` (rguard . (== ""))

prop_regex :: (Reg, String) -> Bool
prop_regex (r, w) = (test r w :: Nub ()) == (testDenot r w :: Nub ())
